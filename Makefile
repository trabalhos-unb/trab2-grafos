CC=g++

all: top.o graph.o stats.o
	$(CC) -std=c++11 -Wall -o program src/main.cpp -I./src src/top.o src/graph.o src/stats.o
top.o: src/top.cpp src/top.hpp
	$(CC) -g -c -std=c++11 -Wall -Wall src/top.cpp -o ./src/top.o
graph.o: src/graph.cpp src/graph.hpp
	$(CC) -g -c -std=c++11 -Wall src/graph.cpp -o ./src/graph.o
stats.o: src/stats.cpp src/stats.hpp
	$(CC) -g -c -std=c++11 -Wall src/stats.cpp -o ./src/stats.o
clean:
	rm src/*.o
run: all
	clear
	./program
