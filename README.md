# Ordenação Topológica em Grafos
Para executar este projeto é necessário ter instalado o gnuplot;

```
sudo apt install gnuplot-x11
```
Após ter instalado o gnuplot, bastar compilar e executar o programa
```
make run
```
