#include "stats.hpp"

void obterDados(const char *nomeArquivo)
{
    Grafo *grafo = Grafo::construirGrafo(nomeArquivo);
    clock_t t;

    t = clock();
    OrdenacaoTopologica::khan(*grafo);
    t = clock() - t;
    float kahn = ((float)t) / CLOCKS_PER_SEC;

    t = clock();
    OrdenacaoTopologica::dfs(*grafo);
    t = clock() - t;
    float dfs = ((float)t) / CLOCKS_PER_SEC;

    std::ofstream arquivoKhanPlot;
    std::ofstream arquivoDfsPlot;

    arquivoKhanPlot.open("dados/graph_khan_exec.txt", std::ios::app);
    arquivoDfsPlot.open("dados/graph_dfs_exec.txt", std::ios::app);

    arquivoKhanPlot << kahn << " " << grafo->quantidadeVertice() << std::endl;
    arquivoDfsPlot << dfs << " " << grafo->quantidadeVertice() << std::endl;

    delete grafo;
}

void Analise::construirGrafico(void)
{
    FILE *fp = popen("gnuplot -persist", "w");
    if (fp == NULL)
    {
        std::cout << "Não foi encontrado uma instalacao do gnuplot." << std::endl;
        return;
    }
    else
    {
        fprintf(fp, "set title 'Comparacao entre algoritmos de ordenacao topologica'\n");
        fprintf(fp, "set autoscale\n");
        fprintf(fp, "set xlabel 'Tempo (segundos)'\n");
        fprintf(fp, "set ylabel 'Nos'\n");
        fprintf(fp, "plot 'dados/graph_khan_exec.txt' lt rgb '#FF0000' with lines smooth mcsplines, \
                      'dados/graph_dfs_exec.txt' lt rgb '#0000FF' with lines smooth mcsplines\n");
    }
}

void Analise::executarAlgoritmos(void)
{
    obterDados("dados/top_small.txt");
    obterDados("dados/top_med.txt");
    obterDados("dados/top_large.txt");
    obterDados("dados/top_huge.txt");
}