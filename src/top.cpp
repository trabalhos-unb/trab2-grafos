#include "top.hpp"

void obterGrauEntrada(Grafo &g, std::vector<int> &ge){
    int quantVertices = g.quantidadeVertice();
    for (int i = 0; i < quantVertices; i++){
        std::list<int>::iterator itr;
        for (itr = g.adjVertice(i).begin(); itr != g.adjVertice(i).end(); itr++){
            ge[*itr]++;
        }
    }
}

void obterVerticesFonte(Grafo &g, std::vector<int> &gev, std::list<int> &vf){
    int quantVertices = g.quantidadeVertice();
    for (int i = 0; i < quantVertices; i++){
        if (gev[i] == 0){
            vf.push_back(i);
        }
    }
}

void OrdenacaoTopologica::khan(Grafo &grafo){
    int v;
    std::list<int> verticesFonte, adjascentes, ordenacao;
    std::vector<int> grauEntrada(grafo.quantidadeVertice(), 0);

    obterGrauEntrada(grafo, grauEntrada);
    obterVerticesFonte(grafo, grauEntrada, verticesFonte);

    while (!verticesFonte.empty()){
        v = verticesFonte.front();
        verticesFonte.pop_front();
        ordenacao.push_back(v);

        adjascentes = grafo.adjVertice(v);
        std::list<int>::iterator itr;
        for (itr = adjascentes.begin(); itr != adjascentes.end(); itr++)
        {
            if (--grauEntrada[*itr] == 0)
            {
                ordenacao.push_back(*itr);
            }
        }
    }
    OrdenacaoTopologica::imprimirOrdenacao(ordenacao);
}

void visitarVertice(Grafo &grafo, int vertice, bool *visitados, std::list<int> &ordenacao)
{
    std::list<int> adjascentes = grafo.adjVertice(vertice);
    visitados[vertice] = true;
    std::list<int>::iterator itr;
    for (itr = adjascentes.begin(); itr != adjascentes.end(); itr++){
        if (visitados[*itr] == false)
        {
            visitarVertice(grafo, *itr, visitados, ordenacao);
        }
    }
    ordenacao.push_front(vertice);
}

void OrdenacaoTopologica::dfs(Grafo &grafo){
    std::list<int> ordenacao;
    int quantVertices = grafo.quantidadeVertice();
    bool visitados[quantVertices];

    for (int i = 0; i < quantVertices; i++) visitados[i] = false;

    for (int v = 0; v < quantVertices; v++){
        if (visitados[v] == false){
            visitarVertice(grafo, v, visitados, ordenacao);
        }
    }
    OrdenacaoTopologica::imprimirOrdenacao(ordenacao);
}

void OrdenacaoTopologica::imprimirOrdenacao(std::list<int>& l){
    std::cout << "Quantidade vertices " << l.size() << '\n';
    for (unsigned int i = 0; i < l.size(); i++){
        std::cout << l.front() << '\t';
        std::cout << std::endl;
        l.pop_front();
    }
}