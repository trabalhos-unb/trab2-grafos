/*****************************************************************************\
|* Teoria e Aplicação de Grafos - A                                          *|
|* Dupla: Danilo Santos - 14/0135910, Bruno Helder - 15/0120338              *|
|*****************************************************************************|
|* Referencias:                                                              *|  
|* http://edirlei.3dgb.com.br/aulas/paa/PAA_Aula_07_Ordenacao_Topologica.pdf *|                                                  *|
|* https://www.geeksforgeeks.org/topological-sorting-indegree-based-solution *|
\****************************************************************************/

#include <iostream>
#include <cstdlib>
#include "stats.hpp"

int main(void)
{
    Analise::executarAlgoritmos();
    Analise::construirGrafico();
    return EXIT_SUCCESS;
}