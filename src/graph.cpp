#include "graph.hpp"

Grafo* Grafo::construirGrafo(const char *nomeArquivo)
{
    Grafo *g;
    std::ifstream arquivo;
    std::string linha;
    int origem, destino;

    arquivo.open(nomeArquivo);
    if (!arquivo.is_open())
    {
        std::cout << "Nao foi possivel abrir o arquivo" << '\n';
        exit(EXIT_FAILURE);
    }
    else
    {
        //Le quantidade de vertices
        getline(arquivo, linha);
        int quantidadeVertices = std::stoi(linha);
        //std::cout << "Tamanho\t" << quantidadeVertices << '\n';
        g = new Grafo(quantidadeVertices);
        //Le vertices
        while (!arquivo.eof())
        {
            getline(arquivo, linha, ' ');
            origem = std::stoi(linha);
            //std::cout << "Vertice\t" << origem << '\n';
            getline(arquivo, linha);
            destino = std::stoi(linha);
            //std::cout << "Aresta\t" << destino << '\n';
            g->adicionarAresta(origem, destino);
        }

        arquivo.close();
    }
    return g;
}

Grafo::Grafo(int quantidadeVertices)
{
    this->listAdj.resize(quantidadeVertices);
}

void Grafo::adicionarAresta(int origem, int destino)
{
    std::list<int> v = this->listAdj[origem];
    v.push_back(destino);
}

int Grafo::quantidadeVertice()
{
    return this->listAdj.size();
}
std::list<int> Grafo::adjVertice(int vertice)
{
    return this->listAdj[vertice];
}