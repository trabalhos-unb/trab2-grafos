#include <iostream>
#include <list>
#include <queue>
#include <stack>
#include "graph.hpp"

namespace OrdenacaoTopologica
{
void khan(Grafo &);
void dfs(Grafo &);
void imprimirOrdenacao(std::list<int>&);
};