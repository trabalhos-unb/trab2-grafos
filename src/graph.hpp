#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <list>
#include <string>

class Grafo{
    std::vector<std::list<int> > listAdj;
  public:
    Grafo(int quantidadeVertices);
    int quantidadeVertice();
    std::list<int> adjVertice(int vertice);
    void adicionarAresta(int , int );
    static Grafo* construirGrafo(const char* );
};